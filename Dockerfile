FROM python:3.6.4-alpine3.6
ENV AUTHLIB_INSECURE_TRANSPORT=1
ENV PYTHONPATH="$PYTHONPATH:/opt/app"
EXPOSE 5000

COPY requirements.txt /opt/app/requirements.txt
WORKDIR /opt/app
RUN apk add --update gcc musl-dev python3-dev libffi-dev openssl-dev && \
    pip install --upgrade pip && \
    pip install --upgrade setuptools && \
    pip install -r requirements.txt && \
    mkdir -p /opt/app/db
COPY cloud /opt/app/cloud

ENTRYPOINT ["python3","/opt/app/cloud/oauth2_server/app.py","0.0.0.0"]
