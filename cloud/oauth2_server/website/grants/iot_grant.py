from cloud.oauth2_server.website.db.models import db
from cloud.oauth2_server.website.db.models import IoTDevice
from cloud.oauth2_server.website.db.models import IoTOAuth2SubToken
from cloud.oauth2_server.website.db.models import IoTSubTokenResourceId
from cloud.third_party.example_oauth2_server.website.models import OAuth2Token
from werkzeug.security import gen_salt
import time


class IoTGrant():

    @staticmethod
    def create_subtoken(request, current_token):
        # Init
        logical_address = request['logical_address']
        physical_address = request['physical_address'] if 'physical_address' in request else None
        user_agent = request['user_agent'] if 'user_agent' in request else None
        geolocation_x = request['geolocation_x'] if 'geolocation_x' in request else None
        geolocation_y = request['geolocation_y'] if 'geolocation_y' in request else None
        device_id = request['device_id'] if 'device_id' in request else None

        # Create IoT device
        iot_device = IoTDevice.query.filter_by(logical_address=logical_address, physical_address=physical_address,
                                               user_agent=user_agent, geolocation_x=geolocation_x,
                                               geolocation_y=geolocation_y, device_id=device_id).first()
        if iot_device is None:
            iot_device = IoTDevice(
                logical_address=logical_address,
                physical_address=physical_address,
                user_agent=user_agent,
                geolocation_x=geolocation_x,
                geolocation_y=geolocation_y,
                device_id=device_id
            )
            db.session.add(iot_device)
            db.session.commit()

        # Create id token
        id_token_code = gen_salt(42)
        iot_oauth2_subtoken = IoTOAuth2SubToken(
            token_type='Bearer',
            id_token=id_token_code,
            scope=current_token.scope,
            revoked=current_token.revoked,
            expires_in=600,     # 10 minutes
            iot_device_id=iot_device.get_iot_id(),
            access_token_id=current_token.id
        )
        db.session.add(iot_oauth2_subtoken)
        db.session.commit()

        # Return
        return {
            'id_token':iot_oauth2_subtoken.id_token,
            'expires_in':iot_oauth2_subtoken.expires_in,
            'token_type':iot_oauth2_subtoken.token_type
        }

    @staticmethod
    def revoke_subtoken(iot_forwarded_header, current_token):
        iot_oauth2_subtoken = IoTOAuth2SubToken.query.filter_by(id_token=iot_forwarded_header).first()
        if iot_oauth2_subtoken is not None and iot_oauth2_subtoken.access_token_id == current_token.id:
            iot_oauth2_subtoken.revoked = 1
            db.session.delete(iot_oauth2_subtoken)
            db.session.add(iot_oauth2_subtoken)
            db.session.commit()
            return True
        return False

    @staticmethod
    def get_iot_device_info(iot_forwarded_header, current_token):
        iot_oauth2_subtoken = IoTOAuth2SubToken.query.filter_by(id_token=iot_forwarded_header).first()
        if iot_oauth2_subtoken is not None:
            if iot_oauth2_subtoken.access_token_id == current_token.id:
                iot_device = IoTDevice.query.filter_by(id=iot_oauth2_subtoken.iot_device_id).first()
                if iot_device is not None and iot_oauth2_subtoken.revoked == 0 and \
                         int(time.time()) < iot_oauth2_subtoken.get_expires_at():
                    return {
                        'logical_address':iot_device.logical_address,
                        'physical_address': iot_device.physical_address,
                        'user_agent': iot_device.user_agent,
                        'geolocation_x': iot_device.geolocation_x,
                        'geolocation_y': iot_device.geolocation_y,
                        'device_id': iot_device.device_id
                    }
                else:
                    return None
            else:
                raise ReferenceError('Id token is not linked with the used access token')
        return None

    @staticmethod
    def get_iot_father(iot_forwarded_header):
        now = int(time.time())
        iot_oauth2_subtoken = IoTOAuth2SubToken.query.filter_by(id_token=iot_forwarded_header).first()
        if iot_oauth2_subtoken is not None and iot_oauth2_subtoken.revoked == 0 and \
                now < iot_oauth2_subtoken.get_expires_at():
            access_token_info = OAuth2Token.query.filter_by(id=iot_oauth2_subtoken.access_token_id).first()
            if access_token_info.revoked == 0 and now < access_token_info.get_expires_at():
                return {
                    'client_id':access_token_info.client_id
                }
        return None

    @staticmethod
    def are_access_token_and_id_token_ok(iot_forwarded_header, current_token):
        now = int(time.time())
        iot_oauth2_subtoken = IoTOAuth2SubToken.query.filter_by(id_token=iot_forwarded_header).first()
        if iot_oauth2_subtoken is not None:
            if iot_oauth2_subtoken.access_token_id == current_token.id and iot_oauth2_subtoken.revoked == 0 and \
               now < iot_oauth2_subtoken.get_expires_at() and current_token.revoked == 0 and \
               now < current_token.get_expires_at():
                return True
        return False

    @staticmethod
    def create_linked_resource_id(iot_forwarded_header):
        now = int(time.time())
        iot_oauth2_subtoken = IoTOAuth2SubToken.query.filter_by(id_token=iot_forwarded_header).first()
        if iot_oauth2_subtoken is not None and iot_oauth2_subtoken.revoked == 0 and \
               now < iot_oauth2_subtoken.get_expires_at():
            # Create resource id
            resource_id = gen_salt(50)
            iot_subtoken_resource_id = IoTSubTokenResourceId(
                resource_id=resource_id,
                id_token_id=iot_oauth2_subtoken.id,
            )
            db.session.add(iot_subtoken_resource_id)
            db.session.commit()
            return resource_id
        raise ReferenceError('Wrong id token')

    @staticmethod
    def is_my_resource_id(iot_forwarded_header, resource_id):
        now = int(time.time())
        iot_oauth2_subtoken = IoTOAuth2SubToken.query.filter_by(id_token=iot_forwarded_header).first()
        if iot_oauth2_subtoken is not None and iot_oauth2_subtoken.revoked == 0 and \
               now < iot_oauth2_subtoken.get_expires_at():
            resource_id = IoTSubTokenResourceId.query.filter_by(id_token_id=iot_oauth2_subtoken.id,
                                                                resource_id=resource_id).first()
            if resource_id is not None:
                return True
        return False
