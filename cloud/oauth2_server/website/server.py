import os
from flask import Flask
from cloud.third_party.example_oauth2_server.website.oauth2 import config_oauth
from cloud.oauth2_server.website.db.models import db
from cloud.oauth2_server.website.routes.routes import bp


class OAuth2Server:

    # -- Public methods

    # OAuth2Server Constructor
    def __init__(self, host='127.0.0.1', port=5000, debug_logging=False):
        super(OAuth2Server, self).__init__()
        self.server_host = host
        self.server_port = port
        self.debug_logging = debug_logging

        # Flask app
        self.app = Flask(__name__, template_folder='../../third_party/example_oauth2_server/website/templates')
        self.app.register_blueprint(bp)

        # load  configuration
        if 'WEBSITE_CONF' in os.environ:
            self.app.config.from_envvar('WEBSITE_CONF')

        self.app.config.update({
            'SECRET_KEY': 'secret',
            'OAUTH2_REFRESH_TOKEN_GENERATOR': True,
            'SQLALCHEMY_TRACK_MODIFICATIONS': False,
            'SQLALCHEMY_DATABASE_URI': 'sqlite:///../../../db/db.sqlite'
        })

        # Config database
        db.init_app(self.app)
        db.create_all(app=self.app)

        # Config OAuth2
        config_oauth(self.app)

    # Runs OAuth2Server
    def run(self):
        self.app.run(host=self.server_host, port=self.server_port, debug=self.debug_logging)
