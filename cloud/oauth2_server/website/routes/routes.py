import json
from flask import request, make_response, g
from authlib.flask.oauth2 import current_token
from cloud.third_party.example_oauth2_server.website.routes import bp
from cloud.oauth2_server.website.grants.iot_grant import IoTGrant
from cloud.third_party.example_oauth2_server.website.oauth2 import require_oauth
from cloud.oauth2_server.decorator.require_oauth_iot import require_oauth_iot


@bp.route('/oauth/sub_token', methods=['POST'])
@require_oauth()
def issue_subtoken():
    data = json.loads(request.data.decode('utf-8'))

    # Check input
    if 'logical_address' not in data or len(data['logical_address']) > 50:
        return __make_response(json.dumps({'err': 400, 'msg': 'Logical address is mandatory and its length should '
                                                              'be minor equal than 50 characters'}, sort_keys=True),
                               400)
    if 'physical_address' in data and len(data['physical_address']) > 50:
        return __make_response(json.dumps({'err': 400, 'msg': 'Physical address length should be minor equal than '
                                                              '50 characters'}, sort_keys=True), 400)
    if 'user_agent' in data and len(data['user_agent']) > 255:
        return __make_response(json.dumps({'err': 400, 'msg': 'User-Agent length should be minor equal than '
                                                              '255 characters'}, sort_keys=True), 400)
    if 'device_id' in data and len(data['device_id']) > 255:
        return __make_response(json.dumps({'err': 400, 'msg': 'Device id length should be minor equal than '
                                                              '255 characters'}, sort_keys=True), 400)
    if ('geolocation_x' in data and 'geolocation_y' not in data) or \
        ('geolocation_x' not in data and 'geolocation_y'  in data) or \
        ('geolocation_x' in data and not isinstance(data['geolocation_x'], float)) or \
        ('geolocation_y' in data and not isinstance(data['geolocation_y'], float)):
        return __make_response(json.dumps({'err': 400, 'msg': 'Wrong geolocation coordinates'}, sort_keys=True), 400)

    # Create subtoken
    id_token = IoTGrant.create_subtoken(data, current_token)

    # Return
    return __make_response(json.dumps(id_token, sort_keys=True), 201)


@bp.route('/oauth/revoke_sub_token', methods=['DELETE'])
@require_oauth()
def revoke_sub_token():
    # Check input
    if 'IoT-Forwarded' in request.headers and request.headers["IoT-Forwarded"] is not None and \
            len(request.headers["IoT-Forwarded"]) > 0 and 'Bearer ' in request.headers["IoT-Forwarded"]:
        iot_forwarded_header = request.headers["IoT-Forwarded"].split(' ')[1]
    else:
        return __make_response(json.dumps({'err': 400, 'msg': 'Missing IoT-Forwarded header'}, sort_keys=True), 400)

    # Revoke token
    revoked = IoTGrant.revoke_subtoken(iot_forwarded_header, current_token)

    # Return
    return __make_response(json.dumps({}), 204) if revoked else \
        __make_response(json.dumps({'err': 404, 'msg': 'Id token not found'}, sort_keys=True), 404)


@bp.route('/api/iot_info', methods=['GET'])
@require_oauth()
def api_iot_info():
    # Check input
    if 'IoT-Forwarded' in request.headers and request.headers["IoT-Forwarded"] is not None and \
            len(request.headers["IoT-Forwarded"]) > 0 and 'Bearer ' in request.headers["IoT-Forwarded"]:
        iot_forwarded_header = request.headers["IoT-Forwarded"].split(' ')[1]
    else:
        return __make_response(json.dumps({'err': 400, 'msg': 'Missing IoT-Forwarded header'}, sort_keys=True), 400)

    # Get IoT device info and return
    try:
        iot_device = IoTGrant.get_iot_device_info(iot_forwarded_header, current_token)
        return __make_response(json.dumps(iot_device, sort_keys=True), 200) if iot_device is not None else \
               __make_response(json.dumps({'err': 404, 'msg': 'IoT device info not found'}, sort_keys=True), 404)
    except ReferenceError:
        __make_response(json.dumps({'err': 403, 'msg': 'Forbidden because access token is not linked with id token'},
                                   sort_keys=True), 403)


@bp.route('/api/iot_father', methods=['GET'])
@require_oauth()
def api_father():
    # Check input
    if 'IoT-Forwarded' in request.headers and request.headers["IoT-Forwarded"] is not None and \
            len(request.headers["IoT-Forwarded"]) > 0 and 'Bearer ' in request.headers["IoT-Forwarded"]:
        iot_forwarded_header = request.headers["IoT-Forwarded"].split(' ')[1]
    else:
        return __make_response(json.dumps({'err': 400, 'msg': 'Missing IoT-Forwarded header'}, sort_keys=True), 400)

    # Get father
    iot_father = IoTGrant.get_iot_father(iot_forwarded_header)

    # Return
    return __make_response(json.dumps(iot_father, sort_keys=True), 200) if iot_father is not None else \
        __make_response(json.dumps({'err': 404, 'msg': 'IoT father info not found'}, sort_keys=True), 404)


def __make_response(payload, code):
    response = make_response(payload, code)
    response.headers["Content-Type"] = "application/json; charset=utf-8"
    return response

# ----------------------------------------------------------------------------------
# ---- Testing APIs are implemented below
# ----------------------------------------------------------------------------------


@bp.route('/events', methods=['POST'])
@require_oauth()
@require_oauth_iot()
def post_new_event():
    resource_id = g.pop('resource_id', None)
    # Resource creation code here ...
    response = __make_response(json.dumps({}, sort_keys=True), 201)
    response.headers["Location"] = "/events/" + resource_id
    return response


@bp.route('/events/<string:id>', methods=['PUT'])
@require_oauth()
@require_oauth_iot()
def put_event(id):
    # Resource modification code here ...
    return __make_response(json.dumps({}, sort_keys=True), 204)


@bp.route('/events/<string:id>', methods=['DELETE'])
@require_oauth()
@require_oauth_iot()
def delete_event(id):
    # Resource modification code here ...
    return __make_response(json.dumps({}, sort_keys=True), 204)


@bp.route('/events/<string:id>', methods=['GET'])
@require_oauth()
@require_oauth_iot()
def get_event(id):
    # Resource get code here ...
    return __make_response(json.dumps({}, sort_keys=True), 200)
