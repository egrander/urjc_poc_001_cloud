import time
from cloud.third_party.example_oauth2_server.website.models import db


class IoTDevice(db.Model):
    __tablename__ = 'iot_device'

    id = db.Column(db.Integer, primary_key=True)
    logical_address = db.Column(db.String(50), nullable=False)
    physical_address = db.Column(db.String(50))
    user_agent = db.Column(db.String(255))
    geolocation_x = db.Column(db.Float)
    geolocation_y = db.Column(db.Float)
    device_id = db.Column(db.String(255))

    def get_iot_id(self):
        return self.id


class IoTOAuth2SubToken(db.Model):
    __tablename__ = 'iot_oauth2_subtoken'

    id = db.Column(db.Integer, primary_key=True)
    token_type = db.Column(db.String(40))
    id_token = db.Column(db.String(255), unique=True, nullable=False)
    scope = db.Column(db.Text, default='')
    revoked = db.Column(db.Boolean, default=False)
    issued_at = db.Column(
        db.Integer, nullable=False, default=lambda: int(time.time())
    )
    expires_in = db.Column(db.Integer, nullable=False, default=0)
    iot_device_id = db.Column(
        db.Integer, db.ForeignKey('iot_device.id', ondelete='CASCADE'))
    iot_device = db.relationship('IoTDevice')
    access_token_id = db.Column(
        db.Integer, db.ForeignKey('oauth2_token.id', ondelete='CASCADE'))
    access_token = db.relationship('OAuth2Token')

    def get_scope(self):
        return self.scope

    def get_expires_in(self):
        return self.expires_in

    def get_expires_at(self):
        return self.issued_at + self.expires_in


class IoTSubTokenResourceId(db.Model):
    __tablename__ = 'iot_subtoken_resource_id'

    id = db.Column(db.Integer, primary_key=True)
    resource_id = db.Column(db.String(255), unique=True, nullable=False)
    id_token_id = db.Column(
        db.Integer, db.ForeignKey('iot_oauth2_subtoken.id', ondelete='CASCADE'))
    id_token = db.relationship('IoTOAuth2SubToken')
