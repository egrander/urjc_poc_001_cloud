import sys
from cloud.oauth2_server.website.server import OAuth2Server

if __name__ == "__main__":
    listen_host = sys.argv[1] if len(sys.argv) > 1 else '127.0.0.1'
    oauth2_server = OAuth2Server(host=listen_host)
    oauth2_server.run()
