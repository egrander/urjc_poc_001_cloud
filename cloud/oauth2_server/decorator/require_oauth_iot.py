import functools
import json
from flask import request, make_response, g
from authlib.flask.oauth2 import current_token
from cloud.oauth2_server.website.grants.iot_grant import IoTGrant


class RequireOAuth2IoT(object):

    def __call__(self):
        def wrapper(f):
            @functools.wraps(f)
            def decorated(*args, **kwargs):
                # Check input
                if 'IoT-Forwarded' in request.headers and request.headers["IoT-Forwarded"] is not None and \
                        len(request.headers["IoT-Forwarded"]) > 0 and 'Bearer ' in request.headers["IoT-Forwarded"]:
                    iot_forwarded_header = request.headers["IoT-Forwarded"].split(' ')[1] # IoT-Forwarded: Bearer <id_token>

                    # Check the relationship between both access token and id token
                    relationship_ok = IoTGrant.are_access_token_and_id_token_ok(iot_forwarded_header, current_token)
                    if not relationship_ok:
                        return RequireOAuth2IoT.__make_response(json.dumps({'err': 401,
                                                'msg': 'Unauthorized because access token is not linked with id token'},
                                                sort_keys=True), 401)

                    # Check base on HTTP method
                    if request.method == 'POST':
                        resource_id = IoTGrant.create_linked_resource_id(iot_forwarded_header)
                        if 'resource_id' not in g:
                            g.resource_id = resource_id
                    elif request.method == 'GET' or request.method == 'PUT' or request.method == 'DELETE':
                        resource_id = request.path.split('/')[-1]
                        # Check if the resource id belong to id token
                        belong = IoTGrant.is_my_resource_id(iot_forwarded_header, resource_id)
                        if not belong:
                            return RequireOAuth2IoT.__make_response(json.dumps({'err': 403,
                                                    'msg': 'Forbidden because resource id does not belong to id token'},
                                                    sort_keys=True), 403)
                    else:
                        # HTTP method not supported
                        pass
                else:
                    # Nothing to do
                    pass
                return f(*args, **kwargs)
            return decorated
        return wrapper

    @staticmethod
    def __make_response(payload, code):
        response = make_response(payload, code)
        response.headers["Content-Type"] = "application/json; charset=utf-8"
        return response


require_oauth_iot = RequireOAuth2IoT()

